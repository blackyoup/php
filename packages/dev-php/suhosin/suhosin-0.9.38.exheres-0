# Copyright 2012-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Suhosin is an advanced protection system for PHP installations"
DESCRIPTION="
Suhosin was designed to protect servers and users from known and unknown flaws
in PHP applications and the PHP core. Suhosin comes in two independent parts,
that can be used separately or in combination. The first part is a small patch
against the PHP core, that implements a few low-level protections against buffer
overflows or format string vulnerabilities and the second part is a powerful PHP
extension that implements all the other protections.
"
HOMEPAGE="http://www.hardened-php.net/${PN}/"
DOWNLOADS="http://download.${PN}.org/${PNV}.tar.gz"

BUGS_TO="philantrop@exherbo.org"
UPSTREAM_CHANGELOG="${HOMEPAGE}/changelog.html [[ lang = en ]]"
LICENCES="PHP-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# The tests are potentially interactive.
RESTRICT="test"

DEPENDENCIES="
    build+run:
        dev-lang/php[>=5.4]
"

src_configure() {
    phpize

    default
}

src_test() {
    export TEST_PHP_EXECUTABLE=/usr/$(exhost --build)/bin/php

    emake -j1 test
}

src_install() {
    # Let install write to PHP exstension directory.
    esandbox allow $(php-config --extension-dir)

    # Provide an updated php.ini which loads the new extension - based on the current php.ini.
    edo mkdir -p "${IMAGE}"/etc/php
    edo cp /etc/php/php.ini "${IMAGE}"/etc/php/php.ini
    hasLines=$(grep -c "extension = ${PN}.so" "${IMAGE}"/etc/php/php.ini)
    if [[ $hasLines -eq 0 ]] ; then
        # Find linenumber extension_dir and use the next line.
        linenum=$(edo grep -n "extension_dir" "${IMAGE}"/etc/php/php.ini | head -1 | awk -F: '{ print $1}')
        linenum=$((linenum+1))
        # Insert extension in php.ini.
        edo sed -i "${linenum}i\extension = ${PN}.so" "${IMAGE}"/etc/php/php.ini
    fi

    default
}

pkg_postinst() {
    # Make sure the extension is executable.
    edo chmod +x $(php-config --extension-dir)/${PN}.so
}

